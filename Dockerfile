FROM python:3

#where in the container we want our app files to live

WORKDIR /app

#what to copy from the host computer and where to copy it to

COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py

RUN pip install -r requirements.txt

CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
