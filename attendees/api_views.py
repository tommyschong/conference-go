from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from .models import Attendee
from events.models import Conference
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    conference = get_object_or_404(Conference, id=conference_id)
    attendees = Attendee.objects.filter(conference=conference)
    attendees_list = []
    for attendee in attendees:
        attendee_data = {
            "name": attendee.name,
            # "href": reverse("attendee_detail", args=[attendee.id]),
        }

        attendees_list.append(attendee_data)
    response_data = {
        "attendees": attendees_list,
    }
    return JsonResponse(
        {"attendees": attendees},
        encoder=AttendeeListEncoder,
    )


def api_show_attendee(request, id):
    attendee = get_object_or_404(Attendee, id=id)
    response_data = {
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created,
        "conference": {
            "name": attendee.conference.name,
            "href": reverse(
                "conference_detail", args=[attendee.conference.id]
            ),
        },
    }
    return JsonResponse(response_data)
