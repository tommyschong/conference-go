from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .keys import PEXELS_API_KEY
from .acls import get_photo, get_weather


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


def api_list_conferences(request):
    conferences = Conference.objects.all()
    return JsonResponse(
        {"conferences": conferences},
        encoder=ConferenceListEncoder,
    )


@require_http_methods(["GET", "POST"])
def api_show_conference(request, id):
    conference = Conference.objects.get(id=id)
    weather = get_weather(
        conference.location.city, conference.location.state.abbreviation
    )
    return JsonResponse(
        {"conference": conference, "weather": weather},
        encoder=ConferenceDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
    try:
        state = State.objects.get(abbreviation=content["state"])
        content["state"] = state
    except State.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )
    else:
        content = json.loads(request.body)

        # Get the State object and put it in the content dict

        state = State.objects.get(abbreviation=content["state"])
        content["state"] = state

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


def api_show_location(request, id):
    location = get_object_or_404(Location, id=id)
    response_data = {
        "name": location.name,
        "city": location.city,
        "room_count": location.room_count,
        "created": location.created,
        "updated": location.updated,
        "state": location.state,
    }
    conference = Conference.objects.get(id=id)

    return JsonResponse(
        {"conference": conference, "weather": weather},
        encoder=ConferenceDetailEncoder,
        safe=False,
    )
